import numpy as np
from itertools import cycle
import itertools
import json
import fileinput
import pandas as pd
import os
import os.path
import csv
from shutil import copyfile
import statistics
import sys
from os import listdir
from os.path import isfile, join
import csv
import re
from scipy.stats.stats import pearsonr
import random
from natsort import natsorted, ns
import operator
from operator import itemgetter


from sklearn.decomposition import PCA, KernelPCA
from sklearn import metrics
#from sklearn.cluster import AffinityPropagation
from sklearn.cluster import KMeans
from sklearn import cluster, mixture
from sklearn.feature_selection import VarianceThreshold
import skfuzzy as fuzz
from sklearn.preprocessing import Normalizer

import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import NullFormatter
from matplotlib.gridspec import GridSpec
from time import time
from sklearn import manifold, datasets
from altair import *
import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
from plotly import tools
from matplotlib.ticker import LinearLocator, FormatStrFormatter

from plotly import __version__
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot, offline


from time import time
from matplotlib import offsetbox
from sklearn import (manifold, datasets, decomposition, ensemble,
                     discriminant_analysis, random_projection)
from numpy.random import rand
from adjustText import adjust_text


