from os import listdir
from os.path import isfile, join


########################################################################################################################
########################################################################################################################
#**********************************************************************************************************************#
#--------------------------------------------------- ERSATZ 2.0 -------------------------------------------------------#
#-------------------------------------------------- RoCS DATASET ------------------------------------------------------#
#**********************************************************************************************************************#
########################################################################################################################
########################################################################################################################



########################################################################################################################
#**********************************************************************************************************************#
#------------------------------------------------ DEFAULT PARAMETERS --------------------------------------------------#
#**********************************************************************************************************************#
########################################################################################################################


#'''
number_of_clusters = 4
# ad-hoc threshold for the acceptable membership value of a property
# TODO - try the sigmoid function to compute the acceptable range of the membership value instead of a step function
representative_property_threshold = 0.30            #0.30
# ad-hoc threshold for the acceptable similarity measure computed using Jaccards coefficient
acceptable_similarity_measure = 0.45        #0.40
#'''

#'''
# substitution model: parameters
r_functional = "relevant_functional_properties"
r_physical = "relevant_physical_properties"
n_substitute = "negative_substitute"
p_substitute = "positive_substitute"
d_role = "designated_role"
e_role = "extended_role"
#'''


########################################################################################################################
#**********************************************************************************************************************#
#--------------------------------------------- Object Property Class Labels -------------------------------------------#
#------------- Create property quality measure labels based on the property class and number of clusters --------------#
#**********************************************************************************************************************#
########################################################################################################################

#'''

# list of functional properties
functional_property_class = ['containment', 'movability', 'blockage', 'support']

# generate instances for each functional property class
functional_properties = []

for f in functional_property_class:
    for i in range(number_of_clusters):
        function = f + '_' + str(i)
        if function not in functional_properties:
            functional_properties.append(function)


# list of physical properties
physical_property_class = ['flatness', 'hollowness', 'size', 'roughness', 'rigidity', 'weight']

# generate instances for each functional property class
physical_properties = []

for p in physical_property_class:
    for i in range(number_of_clusters):
        physical = p + '_' + str(i)
        if physical not in physical_properties:
            physical_properties.append(physical)
#'''


########################################################################################################################
#**********************************************************************************************************************#
# The directory information to store knowledge about instances, object classes, function model and substitution models #
#**********************************************************************************************************************#
########################################################################################################################



#'''
# knowledge base related directories and files
# root directory of the knowledge base
knowledge_base_root = "Knowledge_Base/"

# directory of the instance knowledge
instance_knowledge_file = knowledge_base_root + "object_instance_knowledge.json"

# object class names
obj_class_file = knowledge_base_root + 'object_categories.json'

# object conceptual knowledge
obj_concept_knowledge_file = knowledge_base_root + 'object_concept_knowledge.json'


# knowledge about the known instances and their corresponding known properties
known_obj_instances_track_file = knowledge_base_root + 'known_classes_instances_properties.json'

# knowledge about function model - using K-MEANS
function_model_file = knowledge_base_root + 'function_model.json'

# substitution models file - consists of knowledge about substitutes, relevant properties etc.
substitution_model_file = knowledge_base_root + 'substitution_models_base.json'

#'''


