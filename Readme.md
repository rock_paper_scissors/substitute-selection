#### Background Information
Our approach to *substitute selection* is inspired by the way humans choose a substitute for a missing tool using conceptual knowledge about the objects. For instance, consider a scenario in which one has to choose between a plate and a mouse pad as an alternative for a tray. A tray can be defined as a rigid,rectangular, flat, wooden, brown colored object while a plate can be defined as a rigid, circular, semi-flat, white colored object and a mouse pad as soft, rectangular, flat, leather-based object. Bear in mind, however, that some properties are more relevant than others with respect to the designated purpose of the tool. For a tray whose designated purpose is to carry, rigid and flat are more relevant to carry than a material or a color of a tray. Consequently, to find the
most appropriate substitute, the relevant properties of the unavailable tool need to correspond to as large a degree as possible to the properties of the possible choices for a substitute.  
In this work, a conceptual knowledge-driven computation is performed to identify the relevant properties of the missing tool and determines the most similar substitute on the basis of those properties. Besides identifying the most similar object as a substitute for a missing tool, the approach grants an explicit access to the relevant properties of the missing tool which carries twofold advantages: firstly, knowing which properties are primarily required in the potential substitute narrows down the search space and secondly, in case of an unknown object instance, only the relevant properties will have to be learned to determine a substitute.  
The figure below portrays a typical workflow of the proposed substitute selection approach and it was implemented in Python programming language. The workflow consists of four primary processes: *extract knowledge*, *generate representative models*, *determine relevant properties*, and *determine suitability*.
Refer to our paper[^1] for detailed description of each process.

![ERSATZ Workflow](https://drive.google.com/uc?export=view&id=19KbOTwqZPcWWG5x4J9fMPJW4q_L5TDnj 'Workflow of the proposed substitute selection approach')


[^1]: Thosar, M., Mueller, C. A., Jäger, G., Pfingsthorn, M., Beetz, M., Zug, S., & Mossakowski, T. (2020). Substitute Selection for a Missing Tool Using Robot-Centric Conceptual Knowledge Of Objects. In Knowledge Representation and Reasoning Track in 35th ACM/SIGAPP Symposium On Applied Computing. Brno, Czech Republic.

#### Structure
We have implemented our Substitute Selection approach in Pythin programming language. We have named the system ERSATZ which is a German word for a substitute. As illustrated in the figure below, ERSATZ requires two primary inputs: a missing tool label, labels of the available objects in the environment. 
Based on the primary inputs, ERSATZ extracts the required knowledge from Knowledge Base. ERSATZ determines the suitable substitutes for the missing tool from the available objects. 

![ERSATZ](https://drive.google.com/uc?export=view&id=19sqNNXAQnb6GsoG3cX3EUmnjmPmAKd2m 'ERSATZ Black Box Architecture')


##### Python files:

Python version - 2.7

1. <mark>select_substitute.py</mark> - receives missing tool and available objects labels as input and determines relevant properties of a missing tool and selects substitute/s for a missing tool.  

2. <mark>import_packages.py</mark> - imports the required packages

3. <mark>parameter_declaration.py</mark> - contains the information such as number of clusters, file paths etc.

4. <mark>requirements.txt</mark> - contains the list of all dependencies. Run the following command to install all the dependencies:
```
pip install -r requirements.txt
```

##### Directories:
1. <mark>Knowledge_Base/</mark> : stores the knowledge files such as object instance knowledge, object class knowledge and function model; also stores the substitution model for a missing tool which contains information such as substitutes selected and rejected along with their similarity, relevant physical and functional properties etc.

###### How to select a substitute for a missing tool - Instructions

1. Open the file <mark>select_substitute.py</mark>  

2. Locate the line: <mark>query_object = </mark>  

3. Add any object from the <mark>object_choices</mark> provided in the file as a query object which represents a missing tool  

4. Run the following command on a command line: 
```
python select_substitute.py
```
