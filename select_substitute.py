from import_packages import *
from parameter_declaration import *


def find_substitute_for_query_object(query_object, query_object_task, available_objects):
#def find_substitute_for_query_object(query_object, query_object_task, available_objects,
#                                     representative_property_threshold, acceptable_similarity_measure):

    # load the knowledge base
    with open(obj_concept_knowledge_file, 'r') as qo:
        object_knowledge_base = json.load(qo)

    initiate_object_substitution_model = {r_functional: {},
                                          r_physical: {},
                                          p_substitute: {},
                                          n_substitute: {},
                                          d_role: "empty",
                                          e_role: []
                                          }

    # check whether the knowledge about the queried object exists in the knowledge base
    # in other words whether the object is known
    if query_object not in object_knowledge_base:
        print "The object is not known!"
        print "Initiate learning..."
        # TODO - call the initiate function from the data acquisition framework
        exit()
    else:
        # extract the knowledge about the queried object
        query_object_model = object_knowledge_base[query_object]
        # load the substitution models base
    if os.path.isfile(substitution_model_file):
        with open(substitution_model_file, 'r') as so:
            existing_substitution_model_base = json.load(so)
            # extract the substitution model from the models base if queried object is already present
            if query_object in existing_substitution_model_base:
                query_object_substitution_model = {query_object:{}}
                query_object_substitution_model[query_object].update(existing_substitution_model_base[query_object])
            else:
                query_object_substitution_model = {}
    else:
        existing_substitution_model_base = {}
        query_object_substitution_model = {}


    # check if the objects from the past experience intersect with the current available objects
    if any(query_object_substitution_model):
        # extract the positive substitutes and negative substitutes from the substitution model
        past_p_substitutes = query_object_substitution_model[query_object][p_substitute]
        past_n_substitutes = query_object_substitution_model[query_object][n_substitute]

        # check if any of the available objects were encountered in the past
        # check for the negative experience, it will reduce the number of objects to be checked for substitutability
        if any(past_n_substitutes):
            past_available = set(past_n_substitutes.keys()).intersection(available_objects)
            if past_available:
                available_objects = set(available_objects).difference(past_available)
                if len(available_objects) == 0:
                    print 'All the available objects are not suitable based on the past experience with them.'
                    exit()
        # check if any of the available objects were encountered as positive experience
        if any(past_p_substitutes):
            past_available = set(past_p_substitutes.keys()).intersection(available_objects)
            if past_available:
                print 'ERSATZ had positive experience with the following available objects:'
                # if the available objects contain objects from the past positive experience, return them as substitutes
                return past_available
            # else statement should be here? :what if all the available objects were never encountered in the past??
            # since substitution model of query object already exists, that means, relevant properties are determined.
            # as a result, the following else statement does not make sense
            # which means at this step, the similarity between a query object and available objects should be measured.
            #else:
            #    existing_substitution_model_base.update(compute_relevant_properties(query_object, query_object_model, query_object_substitution_model))
    else:
        # if query object is not present in the substitution model base, then update the model by adding initial knowledge about query object
        existing_substitution_model_base.update({query_object: initiate_object_substitution_model})
        query_object_substitution_model.update({query_object: initiate_object_substitution_model})
        #existing_substitution_model_base[query_object][d_role] = query_object_task
        query_object_substitution_model[query_object][d_role] = query_object_task
        #existing_substitution_model_base.update(compute_relevant_properties(query_object, query_object_model, query_object_substitution_model))
        #query_object_substitution_model = compute_relevant_properties(query_object, query_object_model, query_object_substitution_model)

        ### 3) method three to determine the similarity between a query object and an available object
        representative_property_threshold = compute_representative_threshold(query_object_model)
        #print query_object
        #print representative_property_threshold
        query_object_substitution_model = compute_relevant_properties(query_object, query_object_model,
                                                                      query_object_substitution_model,
                                                                      representative_property_threshold)


        ####




        #existing_substitution_model_base[query_object].update(query_object_substitution_model[query_object])

    #print query_object_substitution_model
    # extract the relevant properties from the query object model
    query_object_relevant_properties = query_object_substitution_model[query_object][r_physical].keys()

    #print query_object_relevant_properties
    # extract the knowledge about each available object and determine the similarity.



    selected_objects = {}
    for obj in available_objects:
        available_object_representative_physical_properties = []
        if obj not in object_knowledge_base:
            print "object "+ obj + " is not known, Initiate Learning..."
            exit()
        # extract the representative physical properties of each available objects
        else:
            # 1) and 2) method one and method two to determine the similarity between a query object and an available object
            #representative_property_threshold = compute_representative_threshold(object_knowledge_base[obj])
            for prop in object_knowledge_base[obj]:
                if prop in physical_properties and object_knowledge_base[obj][prop] > representative_property_threshold:
                    available_object_representative_physical_properties.append(prop)
            # 3) method three to determine the similarity between a query object and an available object
            #for prop in object_knowledge_base[obj]:
            #    if prop in physical_properties and object_knowledge_base[obj][prop] > representative_property_threshold_query_object:
            #        available_object_representative_physical_properties.append(prop)

        ###############################################################################################################
        ### 1) method one to determine the similarity between a query object and an available object
        # extract the representative properties shared by a query object and an available object
        available_object_shared_physical_properties = set(query_object_relevant_properties).intersection(available_object_representative_physical_properties)


        # compute the similarity between an available object and a query object on the basis of shared representative properties
        shared_properties_query_available, similarity_value = jaccard_coefficient(query_object_relevant_properties,
                                                                                  available_object_shared_physical_properties)


        ###############################################################################################################
        if similarity_value > acceptable_similarity_measure:
            if obj not in selected_objects:
                selected_objects.update({obj: similarity_value})
            existing_substitution_model_base[query_object][p_substitute].update({obj: similarity_value})
            # if the obj already exists in the substitution model base, then update the model base by updating the
            # query object task as an extended role of an available object
            #if obj in existing_substitution_model_base:
            #    if query_object_task not in existing_substitution_model_base[obj][e_role]:
            #        existing_substitution_model_base[obj][e_role].append(query_object_task)
            # if the object does not exist in the model base, then add the available object with the initial values
            # add the query object task as extended role
            #else:
            #    existing_substitution_model_base[obj] = initiate_object_substitution_model
        # if the similarity value is below the acceptable value, then add the object as a negative substitute in
        # query object substitution model
        else:
            existing_substitution_model_base[query_object][n_substitute].update({obj: similarity_value})

    # write the update substitution model base

    with open(substitution_model_file, 'w') as swo:
        json.dump(existing_substitution_model_base, swo, indent=4)

    ## uncomment this if the code is not running on ROS

    relevant_f_properties = query_object_substitution_model[query_object]["relevant_functional_properties"]
    relevant_p_properties = query_object_substitution_model[query_object]["relevant_physical_properties"]

    if selected_objects:
        return selected_objects, relevant_f_properties, relevant_p_properties
    else:
        return [0], relevant_f_properties, relevant_p_properties

    #### To run the system on ROS
    #candidates = []
    #sim_val = []
    #if selected_objects:
    #    for obj in selected_objects:
    #       candidates.append(obj)
    #        sim_val.append(selected_objects[obj])
    #    return candidates, sim_val
    #else:
    #    return 'None', 0.0




##########################################################################################################################

#def compute_relevant_properties(query_object, query_object_model, query_object_substitution_model):
### 3) method 3)

def compute_relevant_properties(query_object, query_object_model, query_object_substitution_model, representative_property_threshold):

    #print "Inside relevant property computation-----------------"
    #print "The beginning---------------"
    #print query_object_substitution_model
    # load function model
    with open(function_model_file, 'r') as fmo:
        function_models_base = json.load(fmo)

    # TODO change the name,

    # initiate to extract the physical properties of the query object
    query_object_representative_physical_property = []
    # initiate to extract the functional properties of the query object
    query_object_representative_functional_property = []

    # compute the representative property threshold for query object


    #print "query object " + str(query_object) + " representative threshold: " + str(representative_property_threshold_query_object)

    for prop in query_object_model:
        # only those properties are selected which are greater than a representative property threshold.
        # TODO - the next refinement should include the membership values
        if prop in functional_properties and query_object_model[prop] > representative_property_threshold:
            query_object_representative_functional_property.append(prop)
        elif prop in physical_properties and query_object_model[prop] > representative_property_threshold:
            query_object_representative_physical_property.append(prop)

    #print query_object_representative_physical_property
    #print query_object_representative_functional_property

    #print '##############################################################################################'
    #print query_object
    #print query_object_representative_functional_property
    #print query_object_representative_physical_property
    #print '##############################################################################################'


    # extract all the physical properties associated with a potential relevant functional properties of a query object
    for f in query_object_representative_functional_property:
        function_model_representative_physical_properties = []
        # compute the representative property threshold for a function model of a representative functional property
        #representative_property_threshold_function_model = compute_representative_threshold(function_models_base[f])

        # extract the representative properties of a function model
        for p in function_models_base[f]:
            if function_models_base[f][p] > representative_property_threshold:
                function_model_representative_physical_properties.append(p)


        function_shared_physical_properties = set(query_object_representative_physical_property).intersection(function_model_representative_physical_properties)

        #print '************************************************************************************************'
        #print f
        #print function_model_representative_physical_properties
        #print '************************************************************************************************'

        # compute the similarity between object's physical model and function model
        #print function_model_representative_physical_properties
        #print query_object_representative_physical_property
        relevant_properties, similarity_value = jaccard_coefficient(query_object_representative_physical_property,
                                                                    function_shared_physical_properties)

        #similarity_measure_threshold_relevant_properties = compute_similarity_measure_threshold(query_object_model,
        #                                                                                        function_models_base[f],
        #                                                                                        )

        # check if the functional property is relevant on the basis of similarity value
        if similarity_value > acceptable_similarity_measure:
            # if yes, add the functional properties as relevant in the substitution model
            query_object_substitution_model[query_object][r_functional].update({f: query_object_model[f]})
            # add the physical properties as relevant in the substitution model
            for r in relevant_properties:
                query_object_substitution_model[query_object][r_physical].update({r:query_object_model[r]})

    #print '************************** return **************************'
    #print existing_substitution_model_base
    #print '**************************-------**************************'

    return query_object_substitution_model


##########################################################################################################################

# compute Jaccard's coefficient
def jaccard_coefficient(model_one, model_two):
    model_one_set = set(model_one)
    model_two_set = set(model_two)
    intersection_set = set(model_one_set).intersection(model_two_set)
    union_set = set(model_one_set).union(model_two_set)
    intersection_set_length = len(intersection_set)
    union_set_length = len(union_set)
    if union_set_length != 0:
        similarity_value = round(float(intersection_set_length)/float(union_set_length), 3)
    else:
        similarity_value = 0
    return intersection_set, similarity_value

##########################################################################################################################
#'''
# compute the representative threshold value
def compute_representative_threshold(object_knowledge):

    # initialize threshold variable
    threshold = 0

    # extract the number of properties in the object knowledge
    number_of_properties = len(object_knowledge.keys())

    # make the sum of all the propertion values of the properties of the object
    for prop in object_knowledge:
        threshold = threshold + object_knowledge[prop]

    # check if object knowledge has any properties at all
    if number_of_properties != 0:
        # take the average over all the property values
        representative_threshold = round(float(threshold)/float(number_of_properties), 2)
    else:
        representative_threshold = 0

    return representative_property_threshold
#'''
##########################################################################################################################

##########################################################################################################################
##########################################################################################################################
##########################################################################################################################


#*********************** FOR COMPUTING THE SUBSTITUTE FOR A MISSING TOOL FROM THE AVAILABLE OBJECTS ********************#
with open(obj_class_file, 'r') as co:
    obj_classes = json.load(co)

object_choices = obj_classes.keys()
print "Known object classes are: "
print object_choices
print "--------------------------------------------------------------------------------------------------------"
# object choices = 'paper_box_', 'plastic_box_', 'bowl_', 'to_go_cup_', 'metal_box_', 'tray_', 'plate_', 'cup_', 'sponge_', 'ball_', 'book_'
query_object = 'plastic_box_'
print "Missing tool is: " + query_object
print "--------------------------------------------------------------------------------------------------------"
query_object_task = "none"
available_objects = random.sample(object_choices, 5)
print "Available objects are: "
print available_objects
print "--------------------------------------------------------------------------------------------------------"
print "Substitute for a missing tool is: "
print find_substitute_for_query_object(query_object, query_object_task, available_objects)
print "--------------------------------------------------------------------------------------------------------"

# *********************************************************************************************************************#




